const board = document.getElementById("board");
let divs = board.childNodes;
let color = "black";
let boardTopOffset = board.offsetTop;
let boardLeftOffset = board.offsetLeft;

divs.forEach((e) => {
  e.addEventListener("click", () => {
    e.className = "";
    e.classList.add(color);
  });
});

const clear = document.getElementById("clear");
clear.addEventListener("click", () => {
  divs.forEach((e) => {
    e.className = "";
    e.classList?.add("white");
    color = "black";
  });
});

const colorBtns = document.querySelectorAll(".colorBtn");
colorBtns.forEach((e) => {
  e.addEventListener("click", () => {
    color = e.dataset.color;
  });
});

function convertToCanvas() {
  let positionsDivs = document.querySelectorAll("#board > div");
  const canvas = document.getElementById("canvas");
  ctx = canvas.getContext("2d");
  ctx.fillStyle = "black";
  ctx.lineWidth = 1;

  const offsetArr = [];
  positionsDivs.forEach((e) => {
    offsetArr.push({ top: e.offsetTop - boardTopOffset, left: e.offsetLeft - boardLeftOffset, color: e.className });

    // let pos = e.getBoundingClientRect();

    // console.log(e.className);
    // console.log(pos.x);
    // ctx.fillStyle = e.className;
    // ctx.rect(offset.left, offset.top, 25, 25);
    // ctx.fill();
  });

  // console.log(offsetArr);

  fill(offsetArr);
}

function fill(arr) {
  for (let index = 0; index < arr.length; index++) {
    ctx.fillStyle = arr[index].color;
    let x = arr[index].left;
    let y = arr[index].top;
    ctx.fillRect(x, y, 25, 25);
  }
}

const convertBtn = document.getElementById("convert");
convertBtn.addEventListener("click", convertToCanvas);

function save() {
  // const canvas = document.getElementById("canvas");
  // window.open(canvas.toDataURL("image/png"));

  var canvas = document.getElementById("canvas");
  var dataURL = canvas.toDataURL("image/png");
  var newTab = window.open("about:blank", "image from canvas");
  newTab.document.write("<img src='" + dataURL + "' alt='from canvas'/>");
}

const saveBtn = document.getElementById("save");
saveBtn.addEventListener("click", save);
