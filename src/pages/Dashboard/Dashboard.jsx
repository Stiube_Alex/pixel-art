import React from 'react';
import NavBar from '../../components/NavBar';
import Footer from '../../components/Footer';
import redirect from '../../utils/useRedirect.js';

import { db } from '../../utils/firebase';
import { collection, query, where, getDocs } from 'firebase/firestore';

// import img from '../../assets/preloader.gif';

export default function Dashboard() {
    redirect();
    const postDataArr = [];
    const [posts, setPosts] = React.useState([]);

    const load = async () => {
        const q = query(collection(db, 'posts'));
        const querySnapshot = await getDocs(q);
        querySnapshot.forEach((doc) => {
            postDataArr.push(doc.data());
        });

        postDataArr.sort((a, b) => ~~b.date.split(' - ')[0] - ~~a.date.split(' - ')[0]);
        postDataArr.sort((a, b) => ~~b.date.split(' - ')[1] - ~~a.date.split(' - ')[1]);
        postDataArr.sort((a, b) => ~~b.date.split(' - ')[2] - ~~a.date.split(' - ')[2]);

        for (let index = 0; index < postDataArr.length; index++) {
            const q_ = query(collection(db, 'users'), where('email', '==', postDataArr[index].email));
            const querySnapshot_ = await getDocs(q_);
            querySnapshot_.forEach((doc) => {
                postDataArr[index].email = doc.data().username;
            });
        }

        setPosts(
            postDataArr.map((doc, index) => {
                return (
                    <div key={index} className="postBox">
                        <p className="topPost">{doc.email}</p>
                        <div className="bottomPost">
                            <p className="postTitle">{doc.title}</p>
                            <p className="postDate">{doc.date}</p>
                        </div>
                        <img className="postPhoto" src={doc.image} alt="post" />
                    </div>
                );
            })
        );
    };

    React.useEffect(() => {
        load();
        // eslint-disable-next-line
    }, []);

    return (
        <>
            <NavBar />
            {posts}
            <Footer />
        </>
    );
}
