import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { getAuth, signInWithEmailAndPassword } from 'firebase/auth';

export default function Login() {
    const navigate = useNavigate();

    const email = React.createRef();
    const password = React.createRef();
    const [error, setError] = React.useState();

    const login = async (event) => {
        if (event.key === 'Enter' || event.type === 'click') {
            const auth = getAuth();
            signInWithEmailAndPassword(auth, email.current?.value, password.current?.value)
                .then((userCredential) => {
                    const user = userCredential.user;
                    navigate('/');
                    console.log(user + ' is signed in');
                })
                .catch((error) => {
                    const errorCode = error.code;
                    const errorMessage = error.message;
                    console.log(errorCode);
                    console.log(errorMessage);
                    setError(<p className="logErrorMessage">Wrong email or password</p>);
                });
        }
    };

    return (
        <div>
            <div className="logBox">
                <h2 className="logTitle">Login</h2>
                <div className="logInputsBox">
                    <input ref={email} className="inputLine" placeholder="Email" type="text" />
                    <input
                        ref={password}
                        className="inputLine"
                        placeholder="Password"
                        type="password"
                        onKeyPress={login}
                    />
                </div>
                <div className="midInputs">
                    <button onClick={login}>Log In</button>
                </div>
                {error}
                <div className="botInputs">
                    <Link to="/register">Dont't have an account?</Link>
                    <Link to="/forgot-password">Forgot Password?</Link>
                </div>
            </div>
        </div>
    );
}
