import React from 'react'
import { Link } from 'react-router-dom'

export default function ForgotPassword() {
    return (
        <div>
            {' '}
            <div className="logBox">
                <h2 className="logTitle">Forgot Password</h2>
                <div className="logInputsBox">
                    <input
                        className="inputLine"
                        placeholder="Email"
                        type="text"
                    />
                </div>
                <div className="midInputs">
                    <button>Send</button>
                </div>
                <div className="botInputs">
                    <Link to="/login">Back to Login?</Link>
                </div>
            </div>
        </div>
    )
}
