import React from 'react';
import NavBar from '../../components/NavBar';

import Footer from '../../components/Footer';
import Logout from '../../components/Logout';
import redirect from '../../utils/useRedirect.js';
import profile from '../../assets/profile.png';

import { db } from '../../utils/firebase';
import { collection, query, where, getDocs } from 'firebase/firestore';

export default function Profile() {
    redirect();
    const postDataArr = [];
    const [posts, setPosts] = React.useState([]);
    const [username, setUsername] = React.useState();

    (async () => {
        const q = query(collection(db, 'users'), where('email', '==', localStorage.getItem('currentEmail')));
        const querySnapshot = await getDocs(q);
        querySnapshot.forEach((doc) => {
            setUsername(doc.data().username);
        });
    })();

    const userBox = (
        <div className="userBox">
            <h2>{username}</h2>
            <img className="userPic" src={profile} alt="" />
            <Logout />
        </div>
    );

    const load_ = async () => {
        const data = collection(db, 'posts');
        const q = query(data, where('email', '==', localStorage.getItem('currentEmail')));
        const querySnapshot = await getDocs(q);
        await querySnapshot.forEach((doc) => {
            postDataArr.push(doc);
        });

        postDataArr.sort((a, b) => ~~b.data().date.split(' - ')[0] - ~~a.data().date.split(' - ')[0]);
        postDataArr.sort((a, b) => ~~b.data().date.split(' - ')[1] - ~~a.data().date.split(' - ')[1]);
        postDataArr.sort((a, b) => ~~b.data().date.split(' - ')[2] - ~~a.data().date.split(' - ')[2]);

        setPosts(
            postDataArr.map((doc, index) => {
                return (
                    <div key={index} className="postBox">
                        <div className="bottomPost">
                            <p className="postTitle">{doc.data().title}</p>
                            <p className="postDate">{doc.data().date}</p>
                        </div>
                        <img className="postPhoto" src={doc.data().image} alt="post" />
                    </div>
                );
            })
        );
    };

    React.useEffect(() => {
        load_();
        // eslint-disable-next-line
    }, []);

    return (
        <>
            <NavBar />
            {userBox}
            {posts}
            <Footer />
        </>
    );
}
