import React from 'react';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';

import { db } from '../utils/firebase';
import { getAuth, createUserWithEmailAndPassword } from 'firebase/auth';
import { collection, query, where, getDocs, addDoc } from 'firebase/firestore';

export default function Register() {
    const auth = getAuth();
    const username = React.createRef();
    const email = React.createRef();
    const password = React.createRef();
    const confirmPassword = React.createRef();

    const navigate = useNavigate();
    const [error, setError] = React.useState();

    const register = () => {
        if (username.current.value.length > 2) {
            if (validateEmail(email.current.value.toLowerCase())) {
                if (password.current.value.length > 5) {
                    if (password.current.value === confirmPassword.current.value) {
                        registerUser(username.current.value, email.current.value.toLowerCase(), password.current.value);
                    } else {
                        console.error('Passwords do not match');
                        setError(<p className='regErrorMessage'>Passwords do not match</p>);
                    }
                } else {
                    console.error('Please enter a stronger password');
                    setError(<p className='regErrorMessage'>Please enter a stronger password</p>);
                }
            } else {
                console.error('Please enter a valid email addres');
                setError(<p className='regErrorMessage'>Please enter a valid email addres</p>);
            }
        } else {
            console.error('Please enter a valid username');
            setError(<p className='regErrorMessage'>Please enter a valid username</p>);
        }
    };

    const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/g;
    const validateEmail = (email) => (email.match(emailRegex) ? true : false);

    const registerUser = async (user, email, pass) => {
        const colRef = collection(db, 'users');
        const q = query(colRef, where('username', '==', user));
        const querySnapshot = await getDocs(q);
        if (querySnapshot.docs.length === 0) {
            await createUserWithEmailAndPassword(auth, email, pass)
                .then(() => {
                    addDoc(collection(db, 'users'), { username: user, email: email });
                })
                .catch((error) => {
                    console.log(error);
                });
            console.log('Registered!');
            navigate('/');
        } else {
            console.error('Username Already Taken!');
            setError(<p className='regErrorMessage'>Username Already Taken!</p>);
        }
    };

    return (
        <div>
            <div>
                <div className="logBox">
                    <h2 className="logTitle">Register</h2>
                    <div className="logInputsBox">
                        <input
                            ref={username}
                            className="inputLine"
                            placeholder="Username"
                            type="text"
                            required="required"
                        />
                        <input ref={email} className="inputLine" placeholder="Email" type="text" required="required" />
                        <input
                            ref={password}
                            className="inputLine"
                            placeholder="Password"
                            type="password"
                            required="required"
                        />
                        <input
                            ref={confirmPassword}
                            className="inputLine"
                            placeholder="Confirm Password"
                            type="password"
                            required="required"
                        />
                    </div>
                    {error}
                    <div className="midInputs">
                        <button onClick={register}>Register</button>
                    </div>
                    <div className="botInputs">
                        <Link to="/login">Back to Login?</Link>
                    </div>
                </div>
            </div>
        </div>
    );
}
