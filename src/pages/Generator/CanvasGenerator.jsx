import React, { useRef } from 'react';
import { Link } from 'react-router-dom';
export default function CanvasGenerator() {
    const sizeRef = useRef();
    const colorRef = useRef();

    const sizes = ['16x16', '32x32', '64x64'];
    const color = ['White', 'Black', 'Red', 'Green', 'Blue'];
    const hexColor = {
        white: '#ffffff',
        black: '#000000',
        red: '#ff0000',
        green: '#00ff00',
        blue: '#0000ff',
    };
    window.localStorage.setItem('boardSize', '32');
    window.localStorage.setItem('boardColor', '#ffffff');

    const setColor = (value) => {
        window.localStorage.setItem('boardColor', hexColor[value.toLowerCase()]);
    };

    return (
        <>
            <select
                className="boardGeneratorSelect"
                ref={sizeRef}
                defaultValue={'32x32'}
                onChange={() => window.localStorage.setItem('boardSize', sizeRef.current.value.substring(0, 2))}
                name="size"
            >
                {sizes.map((size) => {
                    return (
                        <option key={size} value={size}>
                            {size}
                        </option>
                    );
                })}
            </select>

            <select
                className="boardGeneratorSelect"
                ref={colorRef}
                defaultValue={'White'}
                onChange={() => setColor(colorRef.current.value)}
                name="color"
            >
                {color.map((color) => {
                    return (
                        <option key={color} value={color}>
                            {color}
                        </option>
                    );
                })}
            </select>
            <Link
                to={{
                    pathname: '/editor',
                }}
            >
                <button>Draw!</button>
            </Link>
        </>
    );
}
