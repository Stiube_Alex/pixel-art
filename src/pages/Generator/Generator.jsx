import React from 'react';
import CanvasGenerator from './CanvasGenerator';
import NavBar from '../../components/NavBar';
import Footer from '../../components/Footer';
import redirect from '../../utils/useRedirect.js';

export default function Generator() {
    redirect();
    return (
        <>
            <NavBar />
            <div className="generatorBox">
                <h2>Generate board</h2>
                <CanvasGenerator />
            </div>
            <div className="footerFixed">
                <Footer />
            </div>
        </>
    );
}
