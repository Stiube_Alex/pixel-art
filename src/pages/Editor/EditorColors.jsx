import React from 'react';

function EditorColors() {
    window.localStorage.setItem('color', 'rgb(0, 0, 0)');

    if (
        window.localStorage.getItem('color') === 'rgb(0, 0, 0)' &&
        window.localStorage.getItem('boardColor') === '#000000'
    ) {
        window.localStorage.setItem('color', 'rgb(255, 255, 255)');
    }

    const colorArr = [
        'rgb(0, 0, 0)',
        'rgb(255, 255, 255)',
        'rgb(255, 0, 0)',
        'rgb(255, 70, 0)',
        'rgb(255, 127, 0)',
        'rgb(254, 179, 0)',
        'rgb(255, 255, 0)',
        'rgb(0, 121, 0)',
        'rgb(0, 255, 0)',
        'rgb(0, 0, 255)',
        'rgb(0, 174, 174)',
        'rgb(115, 8, 165)',
        'rgb(204, 0, 175)',
    ];
    const setColor = (color) => {
        window.localStorage.setItem('color', color);
    };

    const pickerColorChange = (picker) => {
        window.localStorage.setItem('color', picker.target.value);
    };

    const colorDiv = colorArr.map((color) => {
        return (
            <div
                key={color}
                onClick={() => setColor(color)}
                style={{ backgroundColor: color }}
                className={'editorToolButton colorSelectorButton'}
            ></div>
        );
    });

    return (
        <>
            <div>
                {colorDiv}
                <input value={'#dd0000'} onChange={pickerColorChange} className={'colorPicker'} type="color" />
            </div>
        </>
    );
}

export default EditorColors;
