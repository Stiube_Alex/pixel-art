import React from 'react';
import { db } from '../../utils/firebase';
import { getAuth } from 'firebase/auth';
import { collection, addDoc } from 'firebase/firestore';
import { useNavigate } from 'react-router-dom';

import NavBar from '../../components/NavBar';
import EditorTools from './EditorTools.jsx';
import EditorColor from './EditorColors.jsx';
import Footer from '../../components/Footer';
import redirect from '../../utils/useRedirect.js';

export default function Editor() {
    redirect();
    const auth = getAuth();
    const navigate = useNavigate();
    const canvasData = { size: '16', color: 'white' };
    const canvas = React.useRef();
    const pixelBoard = React.useRef();
    const uploadBox = React.useRef();
    const deleteBox = React.useRef();
    const block = React.useRef();
    const titleInput = React.useRef();

    canvasData.size = window.localStorage.getItem('boardSize');
    canvasData.color = window.localStorage.getItem('boardColor');

    const drawPixel = (pos_x, pos_y) =>
        (document.getElementById(`i-${back(pos_x, pos_y)}`).style.backgroundColor =
            window.localStorage.getItem('color'));

    const draw = (event) => {
        switch (window.localStorage.getItem('tool')) {
            case 'pen':
                event.target.style.backgroundColor = window.localStorage.getItem('color');
                break;
            case 'erase':
                event.target.style.backgroundColor = window.localStorage.getItem('boardColor');
                break;
            case 'line':
                line(event);
                break;
            case 'fill':
                floodFill(
                    ~~event.target.id.replace('i-', '') % ~~canvasData.size,
                    findY(~~event.target.id.replace('i-', '')),
                    window.localStorage.getItem('color')
                );
                break;
            default:
                break;
        }
    };

    const dfs = (pos_x, pos_y, old_color, new_color) => {
        if (pos_x < 0 || pos_x >= ~~canvasData.size) {
            return;
        }
        if (pos_y < 0 || pos_y >= ~~canvasData.size) {
            return;
        }
        if (document.getElementById(`i-${back(pos_x, pos_y)}`).style.backgroundColor !== old_color) {
            return;
        }

        drawPixel(pos_x, pos_y);
        dfs(pos_x + 1, pos_y, old_color, new_color); // south
        dfs(pos_x - 1, pos_y, old_color, new_color); // north
        dfs(pos_x, pos_y + 1, old_color, new_color); // east
        dfs(pos_x, pos_y - 1, old_color, new_color); // west
    };

    const floodFill = (pos_x, pos_y, new_color) => {
        const old_color = document.getElementById(`i-${back(pos_x, pos_y)}`).style.backgroundColor;
        if (old_color === new_color) {
            return;
        }
        dfs(pos_x, pos_y, old_color, new_color);
    };

    let firstClick = false;
    let firstX;
    let firstY;
    let secondX;
    let secondY;

    const line = (event) => {
        let nowX = ~~event.target.id.replace('i-', '') % ~~canvasData.size;
        let nowY = findY(~~event.target.id.replace('i-', ''));

        if (firstClick) {
            firstClick = false;
            secondX = nowX;
            secondY = nowY;
            drawLine(firstX, firstY, secondX, secondY);
        } else {
            firstClick = true;
            firstX = nowX;
            firstY = nowY;
        }
    };

    const drawLine = (x1, y1, x2, y2) => {
        let x, y, dx, dy, dx1, dy1, px, py, xe, ye, i;
        dx = x2 - x1;
        dy = y2 - y1;

        if (dx === 0) {
            if (y2 < y1) {
                [y1, y2] = [y2, y1];
            }
            for (y = y1; y <= y2; y++) drawPixel(x1, y);
            return;
        }
        if (dy === 0) {
            if (x2 < x1) {
                [x1, x2] = [x2, x1];
            }
            for (x = x1; x <= x2; x++) drawPixel(x, y1);
            return;
        }

        dx1 = Math.abs(dx);
        dy1 = Math.abs(dy);
        px = 2 * dy1 - dx1;
        py = 2 * dx1 - dy1;

        if (dy1 <= dx1) {
            if (dx >= 0) {
                x = x1;
                y = y1;
                xe = x2;
            } else {
                x = x2;
                y = y2;
                xe = x1;
            }

            drawPixel(x, y);
            for (i = 0; x < xe; i++) {
                x++;
                if (px < 0) {
                    px += 2 * dy1;
                } else {
                    if ((dx < 0 && dy < 0) || (dx > 0 && dy > 0)) {
                        y++;
                    } else {
                        y--;
                    }
                    px += 2 * (dy1 - dx1);
                }
                drawPixel(x, y);
            }
        } else {
            if (dy >= 0) {
                x = x1;
                y = y1;
                ye = y2;
            } else {
                x = x2;
                y = y2;
                ye = y1;
            }

            drawPixel(x, y);
            for (i = 0; y < ye; i++) {
                y++;
                if (py <= 0) {
                    py += 2 * dx1;
                } else {
                    if ((dx < 0 && dy < 0) || (dx > 0 && dy > 0)) {
                        x++;
                    } else {
                        x--;
                    }
                    py += 2 * (dx1 - dy1);
                }

                drawPixel(x, y);
            }
        }
    };

    const findY = (number) => {
        let count = 0;
        while (number > -1) {
            number -= ~~canvasData.size;
            count++;
        }
        return count - 1;
    };

    const back = (pos_x, pos_y) => {
        return pos_x + pos_y * ~~canvasData.size;
    };

    // Inverse third rule
    const scalar = (32 * 15) / ~~canvasData.size;
    const panel = Array.from({
        length: (~~canvasData.size) ** 2,
    }).map((_, index) => {
        const idForDiv = `i-${index}`;
        return (
            <div
                key={index}
                id={idForDiv}
                className="canvasDiv"
                style={{
                    backgroundColor: canvasData.color.toLowerCase(),
                    outline: `1px ${canvasData.color.toLowerCase() === '#000000' ? '#ffffff' : '#000000'} solid`,
                    width: `${scalar}px`,
                    height: `${scalar}px`,
                }}
                onMouseDown={draw}
            ></div>
        );
    });

    const toCanvas = (pixels) => {
        const ctx = canvas.current.getContext('2d');
        const boardTopOffset = pixelBoard.current.offsetTop;
        const boardLeftOffset = pixelBoard.current.offsetLeft;

        ctx.fillStyle = 'black';
        ctx.lineWidth = 1;

        const offsetArr = [];
        pixels.forEach((pixel) => {
            offsetArr.push({
                top: pixel.offsetTop - boardTopOffset,
                left: pixel.offsetLeft - boardLeftOffset,
                color: pixel.style['background-color'],
            });
        });

        for (let index = 0; index < offsetArr.length; index++) {
            ctx.fillStyle = offsetArr[index].color;
            ctx.fillRect(offsetArr[index].left, offsetArr[index].top, scalar, scalar);
        }
    };

    const downloadImage = (pixelList) => {
        toCanvas(pixelList);

        const image = canvas.current.toDataURL('image/png').replace('image/png', 'image/octet-stream');
        const link = document.createElement('a');
        link.download = 'pixel.png';
        link.href = image;
        link.click();
    };

    let converted = false;
    const showConvas = () => {
        if (converted) {
            canvas.current.style.zIndex = -5;
            converted = false;
        } else {
            canvas.current.style.zIndex = 5;
            converted = true;
        }
    };

    const confirmDelete = () => {
        let pixelList = document.querySelectorAll('.canvasDiv');
        pixelList.forEach((element) => {
            element.style.backgroundColor = localStorage.getItem('boardColor');
        });
        hideBox();
    };

    const controlAction = (event) => {
        let pixelList = document.querySelectorAll('.canvasDiv');
        switch (event.target.dataset.key.toLowerCase()) {
            case 'clear':
                deleteBox.current.style.display = 'block';
                block.current.style.display = 'block';
                break;
            case 'post':
                toCanvas(pixelList);
                showUploadBox();
                break;
            case 'download':
                downloadImage(pixelList);
                break;
            case 'grid':
                toCanvas(pixelList);
                showConvas();
                break;
            default:
                break;
        }
    };

    const controlIcons = {
        clear: (
            <svg data-key={'clear'} className="svgSize" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                <path
                    data-key={'clear'}
                    d="M135.2 17.69C140.6 6.848 151.7 0 163.8 0H284.2C296.3 0 307.4 6.848 312.8 17.69L320 32H416C433.7 32 448 46.33 448 64C448 81.67 433.7 96 416 96H32C14.33 96 0 81.67 0 64C0 46.33 14.33 32 32 32H128L135.2 17.69zM394.8 466.1C393.2 492.3 372.3 512 346.9 512H101.1C75.75 512 54.77 492.3 53.19 466.1L31.1 128H416L394.8 466.1z"
                />
            </svg>
        ),
        post: (
            <svg data-key={'post'} className="svgSize" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                <path
                    data-key={'post'}
                    d="M105.4 182.6c12.5 12.49 32.76 12.5 45.25 .001L224 109.3V352c0 17.67 14.33 32 32 32c17.67 0 32-14.33 32-32V109.3l73.38 73.38c12.49 12.49 32.75 12.49 45.25-.001c12.49-12.49 12.49-32.75 0-45.25l-128-128C272.4 3.125 264.2 0 256 0S239.6 3.125 233.4 9.375L105.4 137.4C92.88 149.9 92.88 170.1 105.4 182.6zM480 352h-160c0 35.35-28.65 64-64 64s-64-28.65-64-64H32c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h448c17.67 0 32-14.33 32-32v-96C512 366.3 497.7 352 480 352zM432 456c-13.2 0-24-10.8-24-24c0-13.2 10.8-24 24-24s24 10.8 24 24C456 445.2 445.2 456 432 456z"
                />
            </svg>
        ),
        download: (
            <svg data-key={'download'} className="svgSize" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                <path
                    data-key={'download'}
                    d="M384 128h-128V0L384 128zM256 160H384v304c0 26.51-21.49 48-48 48h-288C21.49 512 0 490.5 0 464v-416C0 21.49 21.49 0 48 0H224l.0039 128C224 145.7 238.3 160 256 160zM255 295L216 334.1V232c0-13.25-10.75-24-24-24S168 218.8 168 232v102.1L128.1 295C124.3 290.3 118.2 288 112 288S99.72 290.3 95.03 295c-9.375 9.375-9.375 24.56 0 33.94l80 80c9.375 9.375 24.56 9.375 33.94 0l80-80c9.375-9.375 9.375-24.56 0-33.94S264.4 285.7 255 295z"
                />
            </svg>
        ),
        grid: (
            <svg data-key={'grid'} className="svgSize" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                <path
                    data-key={'grid'}
                    d="M448 32C483.3 32 512 60.65 512 96V416C512 451.3 483.3 480 448 480H64C28.65 480 0 451.3 0 416V96C0 60.65 28.65 32 64 32H448zM152 96H64V160H152V96zM208 160H296V96H208V160zM448 96H360V160H448V96zM64 288H152V224H64V288zM296 224H208V288H296V224zM360 288H448V224H360V288zM152 352H64V416H152V352zM208 416H296V352H208V416zM448 352H360V416H448V352z"
                />
            </svg>
        ),
    };

    const controlArr = ['clear', 'post', 'download', 'grid'];
    const controlPanel = controlArr.map((control) => {
        return (
            <div key={control} data-key={control} onClick={controlAction} className={'editorToolButton'}>
                {controlIcons[control]}
            </div>
        );
    });

    const showUploadBox = () => {
        uploadBox.current.style.display = 'block';
        block.current.style.display = 'block';
    };

    const hideBox = () => {
        deleteBox.current.style.display = 'none';
        uploadBox.current.style.display = 'none';
        block.current.style.display = 'none';
    };

    const upload = () => {
        if (titleInput.current.value < 1) {
            console.log('give us a title please');
        } else {
            const dataURL = canvas.current.toDataURL();
            let today;

            const date = new Date();
            date.getMonth() + 1 < 10
                ? (today = `${date.getDate()} - 0${date.getMonth() + 1} - ${date.getFullYear()}`)
                : (today = `${date.getDate()} - ${date.getMonth() + 1} - ${date.getFullYear()}`);

            addDoc(collection(db, 'posts'), {
                email: auth.currentUser.email,
                title: titleInput.current.value,
                date: today,
                image: dataURL,
            });

            titleInput.current.value = '';
            hideBox();
            navigate('/', { replace: true });
        }
    };

    return (
        <>
            <NavBar />
            <div ref={uploadBox} className="titleSetBox">
                <h3>Give your piece of art a title!</h3>
                <input ref={titleInput} type="text" className="inputLine" />
                <div>
                    <button onClick={upload}>Upload</button>
                    <button onClick={hideBox}>Cancel</button>
                </div>
            </div>
            <div ref={deleteBox} className="titleSetBox">
                <h3>Are you sure you want to delete this?</h3>
                <div className="fixDeleteButton">
                    <button onClick={confirmDelete}>Delete</button>
                    <button onClick={hideBox}>Cancel</button>
                </div>
            </div>
            <div ref={block} className="block"></div>
            <div className="editorBox">
                <h2 className="editorTitle">Editor</h2>
                <EditorColor />

                <div
                    ref={pixelBoard}
                    className="canvasGrid"
                    style={{
                        gridTemplateColumns: `repeat(${~~canvasData.size}, ${scalar}px)`,
                    }}
                >
                    <canvas
                        ref={canvas}
                        className="canvasStyle"
                        width={~~canvasData.size * scalar}
                        height={~~canvasData.size * scalar}
                    ></canvas>
                    {panel}
                </div>

                <div className="tools">
                    <div className="flexStart">
                        <EditorTools />
                    </div>
                    <div className="flexEnd">{controlPanel}</div>
                </div>
            </div>

            <div className="footerFixed">
                <Footer />
            </div>
        </>
    );
}
