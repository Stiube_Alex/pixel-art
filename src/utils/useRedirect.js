import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { useNavigate } from 'react-router-dom';
let useRedirect;
// eslint-disable-next-line
export default useRedirect = () => {
    const auth = getAuth();
    const navigate = useNavigate();

    onAuthStateChanged(auth, (user) => {
        if (!user) {
            navigate('/login');
            localStorage.setItem('currentEmail', '');
        } else {
            localStorage.setItem('currentEmail', user.email);
        }
    });
};
