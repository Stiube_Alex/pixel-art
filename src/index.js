// React
import React from 'react';
import ReactDOM from 'react-dom';
import Router from './router';

// Style
import './style/style.css';

ReactDOM.render(
    <React.StrictMode>
        <Router />
    </React.StrictMode>,
    document.getElementById('root')
);
