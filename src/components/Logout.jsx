import React from 'react';
import { getAuth, signOut } from 'firebase/auth';

export default function Logout() {
    const auth = getAuth();
    const logout = () => {
        signOut(auth)
            .then(() => {
                console.log('sign out');
            })
            .catch((error) => {
                console.log(error);
            });
    };

    return (
        <div>
            <button onClick={logout}>Logout</button>
        </div>
    );
}
