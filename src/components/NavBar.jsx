import React from 'react';
import { Link } from 'react-router-dom';

export default function NavBar() {
    return (
        <nav>
            <Link to={'/'}>Home</Link>
            <Link to={'/generator'}>Draw</Link>
            <Link to={'/profile'}>Profile</Link>
        </nav>
    );
}
