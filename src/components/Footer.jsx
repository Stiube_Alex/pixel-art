import React from 'react';

export default function Footer() {
    return (
        <footer>
            <p>Copyright © Știube Alexandru</p>
            <p>Created using React, Firebase and Sass</p>
        </footer>
    );
}
